from flask import Flask, jsonify
from flask_cors import CORS
app = Flask(__name__)
cors = CORS(app)

from DockerApi.DockerBlueprint import dockerBp
app.register_blueprint(dockerBp)

@app.route('/')
def welcome():
  response = {
    'message': 'Teste Stone',
    'author': 'João Gabriel Hümmel'
  }
  return jsonify(response)

if __name__ == '__main__':
  app.run(debug=True)