from flask import Response, json
import docker

from ErrorHandler import ErrorHandler

class DockerController:
  client = docker.from_env()

  def containerList(self):
    try:
      containerObjectList = self.client.containers.list(all=True)
      containerList = []
      for container in containerObjectList:
        containerInfo = {
          'id': container.id,
          'short_id': container.short_id,
          'name': container.name,
          'image': container.image.tags[0],
          'status': container.status,
          'labels': container.labels,
        }
        containerList.append(containerInfo)
      response = containerList
      status = 200
    except Exception as exception:
      response, status = ErrorHandler.defaultError(exception)
    return response, status
  
  def containerStart(self, id):
    try:
      container = self.client.containers.get(id)
      container.start()
      response = {
        'message': f'Container "{id}" iniciado com sucesso.'
      }
      status = 200
    except docker.errors.NotFound:
      response = {
        'message': f'Container "{id}" não encontrado.'
      }
      status = 404
    except Exception as exception:
      response, status = ErrorHandler.defaultError(exception)
    return response, status
  
  def containerStop(self, id):
    try:
      container = self.client.containers.get(id)
      container.stop()
      response = {
        'message': f'Container "{id}" parado com sucesso.'
      }
      status = 200
    except docker.errors.NotFound:
      response = {
        'message': f'Container "{id}" não encontrado.'
      }
      status = 404
    except Exception as exception:
      response, status = defaultError(exception)
    return response, status

  def containerRun(self, request):
    try:
      self.client.containers.run(**request)
      response = {
        'message': f'Container "{request["name"]}" criado com sucesso.'
      }
      status = 201
    except docker.errors.APIError as exception:
      status = exception.response.status_code
      def runErrorSwitch(value):
        return {
            '400': 'Requisição mal feita',
            '404': 'A imagem escolhida não foi encontrada',
            '409': 'Já existe um container com este nome',
        }.get(value, 'docker.errors.APIError: Erro não mapeado.')
      response = {
        'message': runErrorSwitch(str(status))
      }
    except Exception as exception:
      response, status = ErrorHandler.defaultError(exception)
    return response, status
  
  def containerRemove(self, id):
    try:
      container = self.client.containers.get(id)
      container.remove(force=True)
      response = {
      'message': f'Container "{id}" removido com sucesso.'
      }
      status = 200
    except docker.errors.NotFound:
      response = {
        'message': f'Container "{id}" não encontrado.'
      }
      status = 404
    except Exception as exception:
      response, status = ErrorHandler.defaultError(exception)
    return response, status