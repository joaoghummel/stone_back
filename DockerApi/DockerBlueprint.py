from flask import Blueprint, Response, request, json
import docker

from .DockerController import DockerController

from ResponseHandler import ResponseHandler

dockerBp = Blueprint('docker', __name__, url_prefix='/docker')
controller = DockerController()

@dockerBp.route('container/list')
def containerList():
  response, status = controller.containerList()
  return ResponseHandler.defaultResponse(response, status)

@dockerBp.route('container/start/<id>')
def containerStart(id):
  response, status = controller.containerStart(id)
  return ResponseHandler.defaultResponse(response, status)

@dockerBp.route('container/stop/<id>')
def containerStop(id):
  response, status = controller.containerStop(id)
  return ResponseHandler.defaultResponse(response, status)

@dockerBp.route('container/run', methods=['POST'])
def containerRun():
  response, status = controller.containerRun(request.json)
  return ResponseHandler.defaultResponse(response, status)

@dockerBp.route('container/remove/<id>')
def containerRemove(id):
  response, status = controller.containerRemove(id)
  return ResponseHandler.defaultResponse(response, status)