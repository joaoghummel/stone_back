# Instruções para rodar o projeto

## Requisitos
- Python 3
- pip3
- virtualenv
- docker

## Testando a aplicação
- Acessa caminho da aplicação onde o projeto foi clonado.
- Criar um virtualenv: virtualenv venv
- Ativar o virtualenv: . venv/bin/activate
- Instalar requirements.txt: pip3 install -r requirements.txt
- Iniciar servidor local: python3 main.py

Agora você pode acessar: http://localhost:5000

## Possíveis rotas
- **GET:** http://localhost:5000/
- **POST:** http://localhost:5000/docker/container/run
  ```
  Exemplo de envio:
  {
    "image": "debian:latest",
    "name": "nome do container",
    "detach": true,
    "tty": true,
    "command": "/bin/bash"
  }
  ```
- **GET:** http://localhost:5000/docker/container/list
- **GET:** http://localhost:5000/docker/container/start/**id** => Substituir **id** da url pelo id ou nome do container
- **GET:** http://localhost:5000/docker|container/stop/**id** => Substituir **id** da url pelo id ou nome do container
- **GET:** http://localhost:5000/docker/container/remove/**id** => Substituir **id** da url pelo id ou nome do container