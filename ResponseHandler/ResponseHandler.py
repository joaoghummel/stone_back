from flask import Response, json

def defaultResponse(response, status, mimetype='application/json'):
  if mimetype == 'application/json':
    return Response(json.dumps(response), status=status, mimetype=mimetype)
  else:
    return Response(response, status=status, mimetype=mimetype)