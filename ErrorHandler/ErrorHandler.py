def defaultError(exception):
  if(hasattr(exception, 'response')):
    status = exception.response.status_code
  else:
    status = 500
  error = {
    'message': 'Erro não mapeado.',
    'error': str(exception)
  }
  return error, status